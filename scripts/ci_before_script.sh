#!/usr/bin/env sh

set -eu

apt -yqq update
apt -yqq install ocaml opam shellcheck m4 libgtk2.0-dev ocaml-native-compilers \
    camlp4-extra ocamlbuild devscripts dh-ocaml git-buildpackage git

opam init -a
# shellcheck disable=SC2046
eval `opam config env`
opam update -y
eval `opam config env`
CHECK_IF_PREINSTALLED=false opam install -y alcotest lablgtk ocamlfind yojson
eval `opam config env`
